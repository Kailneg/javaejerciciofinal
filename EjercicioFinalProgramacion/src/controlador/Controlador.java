package controlador;

import gui.*;

public class Controlador {
	
	private Inicio ventanaInicio;
	private Cronometro ventanaCronometro;
	private Suplementos ventanaSuplementos;
	private CajaDelDia ventanaCaja;
	
	
	public Controlador() {
		ventanaInicio = new Inicio(this);
		mostrarInicio(true);
	}

	
	public void mostrarInicio(boolean visible) {
		if (ventanaInicio == null)
			ventanaInicio = new Inicio(this);
		ventanaInicio.setVisible(visible);
	}
	
	public void mostrarCronometro(boolean visible) {
		if (ventanaCronometro == null)
			ventanaCronometro = new Cronometro(this);
		ventanaCronometro.setVisible(visible);
	}
	
	public void mostrarSuplementos(boolean visible) {
		if (ventanaSuplementos == null)
			ventanaSuplementos = new Suplementos(this);
		ventanaSuplementos.setVisible(visible);
	}
	
	public void mostrarCaja(boolean visible) {
		if (ventanaCaja == null)
			ventanaCaja = new CajaDelDia(this);
		ventanaCaja.setVisible(visible);
	}
}

