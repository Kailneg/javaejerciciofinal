package commons;

import java.util.Arrays;
import java.util.Scanner;

public class Cola<T> {
	// ATRIBUTOS
	private T[] Elementos;
	private Boolean lifo = false;

	// CONSTRUCTOR VAC�O

	/*
	 * Reservar� memoria para un array de 0 T elemento.
	 */
	public Cola() {

		this.Elementos = (T[]) new Object[0];
		this.lifo = false;
	}

	// GETTER AND SETTERS

	public Boolean getLifo() {
		return lifo;
	}

	public void setLifo(Boolean lifo) {
		this.lifo = lifo;
	}

	// M�TODOS

	/*
	 * Activar/desactivar lifo
	 */
	public void Lifo() {

		if (!getLifo())
			this.lifo = true;
		else
			this.lifo = false;
	}

	/*
	 * A�ade elemento al array.Lo a�ade al final
	 */
	public void push(T element) {

		if(!Comprobacion(element))
		{
		int aux = size();
		ResizeM();
		this.Elementos[aux] = element;
		}
		else
		{
		System.out.println("Ya se encuentra en la cola el elemento");
		System.out.println(element.toString());
		}
	}

	/*
	 * Comprueba si existe el elemento en el cajon
	 */
	public Boolean Comprobacion(T element){
		Boolean existe=false;
		
		for (T t : this.Elementos) {
	
			if(t.toString().equals(element.toString()))
			{
				existe=true;
			}
			
		}
		
		return existe;
	}
	
	/*
	 * Elimina un elemento elegido
	 */
	public void remove(T element) {

		if (this.Elementos.length > 0) {

			for (int i = 0; i < this.Elementos.length; i++) {
				if (this.Elementos[i].equals(element)) {
					this.Elementos[i] = null;
				}
			}
			ResizeC();
		} else {
			System.out.println("No hay elementos en la cola");
		}

	}

	/*
	 * Selecciona un elemento del array
	 */
	public T select() {

		int opcion = 0;
		T t = null;

		if (this.Elementos.length > 0) {
			MostrarCola();
			
			opcion = opcion();
			t = this.Elementos[opcion];
			return t;
		} else {
			return null;
		}

	}

	/*
	 * Funcion para leer un int
	 */
	public int opcion() {
		Scanner sc = new Scanner(System.in);
		String aux;
		int opcion = 0;
		boolean correcto = false;

		while (!correcto) {
			System.out.println("Que Elemento quiere eliminar:");
			aux = sc.nextLine();
			try {
				opcion = Integer.parseInt(aux);
				if (opcion <= 0 || opcion > this.Elementos.length) {
					System.out.println("Esa opci�n no existe");
					correcto = false;
				} else {
					correcto = true;
				}
			} catch (Exception e) {
				System.out.println("Inserte un n�mero");
				correcto = false;
			}
		}

		return opcion - 1;
	}

	/*
	 * Elimina el siguiente elemento del array
	 */
	public T pop() {
		int aux = size();
		T t;

		if (!getLifo()) {
			if ( !isEmpty() && this.Elementos[0] != null) {
				t = this.Elementos[0];
				this.Elementos[0] = null;
				ResizeC();
				return t;
			} else {
				System.out.println("No hay elementos");
				return null;
			}
		} else {
			if ( !isEmpty() && this.Elementos[size()-1] != null) {
				t = this.Elementos[size()-1];
				this.Elementos[size()-1] = null;
				ResizeC();
				return t;
			} else {
				System.out.println("No hay elementos");
				return null;
			}
		}
	}

	/*
	 * Elimina todo del array y lo redimensiona
	 */
	public void clear() {

		while (this.Elementos.length > 0) {
			for (int i = 0; i < this.Elementos.length; i++) {
				this.Elementos[i] = null;
			}
			ResizeC();
		}
	}

	/*
	 * Devuelve true si el elemnento se encuentra en la colecci�n
	 */
	public boolean contains(T t) {

		Boolean aux = false;

		for (int i = 0; i < this.Elementos.length; i++) {
			if (Elementos[i].equals(t)) {
				aux = true;
			}
		}

		return aux;
	}


	
	
	/*
	 * Devuelve true si el array est� vacio
	 */
	public boolean isEmpty() {
		int contador = 0;

		for (int i = 0; i < this.Elementos.length; i++) {
			contador++;
		}

		if (contador == 0)
			return true;
		else
			return false;
	}

	/*
	 * Devuelve el tama�o de la cola
	 */
	public int size() {

		return this.Elementos.length;

	}

	/*
	 * Redimensionar el array a�adiendo una posicion
	 */
	public void ResizeM() {
		this.Elementos = Arrays.copyOf(Elementos, Elementos.length + 1);
	}

	/*
	 * Redimensionar el array eliminando una posicion
	 */
	public void ResizeN() {
		this.Elementos = Arrays.copyOf(Elementos, Elementos.length - 1);
	}

	/*
	 * Redimensiona el array quitando los nulls y restando las posiciones
	 */
	public void ResizeC() {
		for (int i = 0; i < Elementos.length; i++) {
			if (Elementos[i] == null) {
				for (int x = i; x < Elementos.length; x++) {
					if (x + 1 < Elementos.length) {
						Elementos[x] = Elementos[x + 1];
					}
				}
			}

		}
		ResizeN();

	}

	/*
	 *Mostrar Cola 
	 */
	public void MostrarCola()
	{
		if (this.Elementos.length > 0) {
			for (int i = 0; i < this.Elementos.length; i++) {
				if (this.Elementos[i] != null) {
					System.out.println((i + 1) + ". " + this.Elementos[i].toString());
				}
			}
		}else
		{
			System.out.println("La cola est� vac�a");
		}
	}
	
	/*
	 * Opcion a elegir en el menu
	 */
	public int opcionMenu() {
		Scanner sc = new Scanner(System.in);
		int opcion = 0;
		boolean correcto = false;
		String aux, men;

		if (getLifo())
			men = "Lifo";
		else
			men = "Fifo";
		while (!correcto) {
			System.out.println("******MENU DE LA COLA******");
			System.out.println("El programa se encuentra en " + men);
			System.out.println();
			System.out.println("1.Aniadir Persona/Animal");
			System.out.println("2.Cuanto mide la cola");
			System.out.println("3.Vaciar la cola");
			System.out.println("4.Eliminar a alguien de la cola");
			System.out.println("5.Eliminar al siguiente de la cola");
			System.out.println("6.Mostrar datos de quien se encuentra en la cola");
			System.out.println("7.Activa/desactivar Lifo");
			System.out.println("8.Salir");
			aux = sc.nextLine();
			try {
				opcion = Integer.parseInt(aux);
				if (opcion < 1 || opcion > 8) {
					System.out.println("Inserte una opci�n del menu");
				} else {
					correcto = true;
				}
			} catch (Exception e) {
				correcto = false;
				System.out.println("Inserta un n�mero");
			}
		}

		return opcion;
	}

}

