package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import commons.Carrera;
import commons.Contenedor;
import commons.ListaOrdenadaGenerica;
import controlador.Controlador;

import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class CajaDelDia extends JFrame {

	private JPanel contentPane;
	private ListaOrdenadaGenerica<Carrera> lista;
	private JLabel lblEuros;
	private Controlador controlador;
	private JButton btnNewButton;

	/**
	 * Create the frame.
	 */
	public CajaDelDia(Controlador controlador) {
		this.controlador = controlador;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblTotal = new JLabel("Total:");
		lblTotal.setBounds(48, 114, 46, 14);
		contentPane.add(lblTotal);
		
		lblEuros = new JLabel("");
		lblEuros.setBounds(104, 114, 133, 14);
		contentPane.add(lblEuros);
		
		btnNewButton = new JButton("Atras");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				setVisible(false);
				controlador.mostrarInicio(true);
			}
		});
		btnNewButton.setBounds(48, 194, 360, 67);
		contentPane.add(btnNewButton);
	}
	
	@Override
	public void setVisible(boolean b) {
		calcularCaja();
		super.setVisible(b);
	}

	private void calcularCaja() {
		float totalCaja = 0f;;
		lista = Contenedor.getInstance().getLista();
		
		for (Carrera c : lista) {
			totalCaja += c.getPrecioTotal();
		}
		
		lblEuros.setText(totalCaja + "�");
	}

}
