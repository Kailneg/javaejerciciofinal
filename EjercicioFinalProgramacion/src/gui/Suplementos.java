package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import commons.Carrera;
import commons.Contenedor;
import controlador.Controlador;

import java.awt.GridLayout;
import javax.swing.JToggleButton;
import java.awt.Color;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import java.util.Calendar;
import java.beans.PropertyChangeEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Suplementos extends JFrame {

	private JPanel contentPane;
	private JTextField txtDinerocliente;
	private JToggleButton tglbtnNocturnidad;
	private JToggleButton tglbtnAeropuerto;
	private JToggleButton tglbtnFinDeSemana;
	private JPanel panel;
	private JPanel panel_1;
	private JLabel lblTotal;
	private JPanel panel_2;
	private JPanel panel_3;
	private JLabel lblCambio;
	private JButton btnPagado;
	private Controlador controlador;
	private float precioTotal;

	/**
	 * Create the frame.
	 */
	public Suplementos(Controlador controlador) {
		this.controlador = controlador;
		setTitle("Suplementos");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);

		tglbtnNocturnidad = new JToggleButton("Nocturnidad");
		
		tglbtnAeropuerto = new JToggleButton("Aeropuerto");
		tglbtnFinDeSemana = new JToggleButton("Fin de semana");

		panel = new JPanel();
		panel_1 = new JPanel();
		lblTotal = new JLabel("Total");
		panel_2 = new JPanel();
		txtDinerocliente = new JTextField();
		panel_3 = new JPanel();
		lblCambio = new JLabel("Cambio");
		btnPagado = new JButton("Pagado");
		contentPane = new JPanel();

		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(5, 0, 0, 0));

		contentPane.add(tglbtnNocturnidad);

		contentPane.add(tglbtnAeropuerto);

		contentPane.add(tglbtnFinDeSemana);

		contentPane.add(panel);
		panel.setLayout(new GridLayout(0, 3, 0, 0));

		panel_1.setBackground(new Color(153, 204, 0));
		panel_1.setForeground(new Color(255, 102, 0));
		panel.add(panel_1);
		panel_1.setLayout(new BorderLayout(0, 0));

		lblTotal.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panel_1.add(lblTotal, BorderLayout.CENTER);

		panel.add(panel_2);
		panel_2.setLayout(new BorderLayout(0, 0));


		txtDinerocliente.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panel_2.add(txtDinerocliente);
		txtDinerocliente.setColumns(10);

		panel_3.setBackground(new Color(153, 255, 153));
		panel.add(panel_3);
		panel_3.setLayout(new BorderLayout(0, 0));

		lblCambio.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panel_3.add(lblCambio, BorderLayout.CENTER);
		contentPane.add(btnPagado);
		
		adaptadores();
	}
	
	@Override
	public void setVisible(boolean b) {
		calcularTotal();
		super.setVisible(b);
	}
	

	private void calcularTotal() {
		final float precioSegundoSemana = 0.003f;
		final float precioSegundoFinde = 0.0043333f;
		
		precioTotal = 0f;
		
		boolean esFinSemana, esNocturnidad, esAeropuerto;
		esFinSemana = tglbtnFinDeSemana.isSelected();
		esNocturnidad = tglbtnNocturnidad.isSelected();
		esAeropuerto = tglbtnAeropuerto.isSelected();
		
		long segundosTotal = Contenedor.getInstance().getTiempoTotal().getTimeInMillis() / 1000;
		precioTotal = (esFinSemana) ? (4.55f + segundosTotal * precioSegundoFinde) : (3.65f + segundosTotal * precioSegundoSemana);
		
		if (esNocturnidad)
			precioTotal += 2;
		if (esAeropuerto)
			precioTotal += 5.5;
		
		lblTotal.setText(precioTotal + "�");
	}

	private void adaptadores() {
		tglbtnNocturnidad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calcularTotal();
			}
		});
		tglbtnFinDeSemana.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calcularTotal();
			}
		});
		tglbtnAeropuerto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calcularTotal();
			}
		});
		
		//texto Cliente
		txtDinerocliente.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				float dinero = Float.parseFloat(txtDinerocliente.getText() + e.getKeyChar());
				lblCambio.setText((dinero - precioTotal) + "�");
			}
		});
		
		//boton pagado
		btnPagado.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				Carrera carrera = new Carrera(Contenedor.getInstance().getHoraEntrada(), 
						Contenedor.getInstance().getHoraSalida(), precioTotal);
				Contenedor.getInstance().getLista().add(carrera);
				setVisible(false);
				controlador.mostrarInicio(true);
			}
		});
		
	}

}
