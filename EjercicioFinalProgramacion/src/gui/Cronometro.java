package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import commons.Contenedor;
import controlador.Controlador;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Calendar;
import java.util.Date;

public class Cronometro extends JFrame {

	private JPanel contentPane;
	private Controlador controlador;
	private JTextField txt_inicio;
	private JTextField txt_fin;
	private JLabel lblHoraInicio;
	private JLabel lblHoraFin;
	private JButton btnFin;
	private Calendar horaInicio, horaFin, tiempoTotal;
	private JLabel lblTiempoTotal;
	private JLabel lblTiempo;
	private JButton btnPasarSiguiente;


	/**
	 * Create the frame.
	 */
	public Cronometro(Controlador controlador) {
		this.controlador = controlador;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		btnFin = new JButton("Fin de Trayecto");
		btnFin.setBounds(10, 165, 414, 85);
		contentPane.add(btnFin);
		
		lblHoraInicio = new JLabel("Hora Inicio:");
		lblHoraInicio.setBounds(10, 11, 200, 14);
		contentPane.add(lblHoraInicio);
		
		txt_inicio = new JTextField();
		txt_inicio.setBounds(10, 36, 143, 20);
		contentPane.add(txt_inicio);
		txt_inicio.setColumns(10);
		
		lblHoraFin = new JLabel("Hora Fin:");
		lblHoraFin.setBounds(10, 67, 200, 14);
		contentPane.add(lblHoraFin);
		
		txt_fin = new JTextField();
		txt_fin.setBounds(10, 92, 143, 20);
		contentPane.add(txt_fin);
		txt_fin.setColumns(10);
		
		lblTiempoTotal = new JLabel("Tiempo total:");
		lblTiempoTotal.setBounds(181, 11, 96, 14);
		contentPane.add(lblTiempoTotal);
		
		lblTiempo = new JLabel("Tiempo");
		lblTiempo.setBounds(181, 39, 46, 14);
		contentPane.add(lblTiempo);
		
		btnPasarSiguiente = new JButton("Pasar siguiente");

		btnPasarSiguiente.setEnabled(false);
		btnPasarSiguiente.setBounds(181, 67, 243, 87);
		contentPane.add(btnPasarSiguiente);
		adaptadores();
	}
	
	@Override
	public void setVisible(boolean b) {
		horaInicio = Calendar.getInstance();
		txt_inicio.setText(horaInicio.get(Calendar.HOUR_OF_DAY) + ":" + horaInicio.get(Calendar.MINUTE) + ":" + 
				horaInicio.get(Calendar.SECOND));
		super.setVisible(b);
	}


	private void adaptadores() {
		//Boton fin
		btnFin.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				horaFin = Calendar.getInstance();
				tiempoTotal = Calendar.getInstance();
				txt_fin.setText(horaFin.get(Calendar.HOUR_OF_DAY) + ":" + horaFin.get(Calendar.MINUTE) + ":" + 
						horaFin.get(Calendar.SECOND));
				
				Date d = new Date(horaFin.getTimeInMillis() - horaInicio.getTimeInMillis());
				
				tiempoTotal.setTime(d);
				
				lblTiempo.setText((tiempoTotal.get(Calendar.HOUR_OF_DAY)-1) + ":" + tiempoTotal.get(Calendar.MINUTE) + ":" + 
						tiempoTotal.get(Calendar.SECOND));
				btnPasarSiguiente.setEnabled(true);
			}
		});
		
		//Boton siguiente
		btnPasarSiguiente.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				Contenedor.getInstance().setHoraEntrada(horaInicio);
				Contenedor.getInstance().setHoraSalida(horaFin);
				Contenedor.getInstance().setTiempoTotal(tiempoTotal);
				txt_fin.setText("");
				btnPasarSiguiente.setEnabled(false);
				setVisible(false);
				controlador.mostrarSuplementos(true);
			}
		});
	}
}
