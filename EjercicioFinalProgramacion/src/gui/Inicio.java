package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.Controlador;

import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Inicio extends JFrame {

	private JPanel contentPane;
	private JButton btn_subida;
	private JButton btn_caja;
	private Controlador controlador;


	/**
	 * Create the frame.
	 */
	public Inicio(Controlador controlador) {
		this.controlador = controlador;
		setTitle("Inicio");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(0, 2, 0, 0));

		btn_subida = new JButton("Subida del viajero");
		btn_caja = new JButton("Caja del d\u00EDa");

		contentPane.add(btn_subida);
		contentPane.add(btn_caja);

		adaptadores();
	}

	private void adaptadores() {
		// Subida viajero
		btn_subida.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				setVisible(false);
				controlador.mostrarCronometro(true);
			}
		});
		
		//Caja del d�a
		btn_caja.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				setVisible(false);
				controlador.mostrarCaja(true);
			}
		});
	}

}
